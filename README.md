# Node.js TS3 ServerQuery Client

A TS3 ServerQuery interface written in JavaScript.

## Setup/Requirements
Requires Node.js version 10.6+

Start using `npm start`

## Plugins
- [IdleCheck](https://github.com/weedz/node-ts3_idlecheck)

### Documentation
Take a look at [ExamplePlugin](https://github.com/weedz/node-ts3_exampleplugin).
